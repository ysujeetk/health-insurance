import java.text.NumberFormat;


/**
 * 
 * @author Sujeet Kumar
 *
 */
public class PremiumQuoteGenerator {

	public static int premiumCalculation(Customer customer) {
		
		//Base premium for anyone below the age of 18 years = Rs. 5,000
		double result = 5000d;
		
		result = getAgeBasedCalcuation(customer, result);
		
				
		if (customer.getGender() == 'm') {
			result += result * 2 / 100;
		}
		
		
		int preexisting = 0;
		
		preexisting += Boolean.valueOf(customer.isHypertension()).compareTo(false);
		preexisting += Boolean.valueOf(customer.isBloodPressure()).compareTo(false);
		preexisting += Boolean.valueOf(customer.isBloodSugar()).compareTo(false);
		preexisting += Boolean.valueOf(customer.isOverweight()).compareTo(false);
		
		result += result * preexisting / 100;
		
		int habits = 0;
		
		//Good Habits
		habits -= (Boolean.valueOf(customer.isDailyExercise()).compareTo(false))*3;
		//Bad Habits
		habits += (Boolean.valueOf(customer.isSmoking()).compareTo(false))*3; 
		habits += (Boolean.valueOf(customer.isAlcohol()).compareTo(false))*3; 
		habits += (Boolean.valueOf(customer.isDrugs()).compareTo(false))*3; 
				
		result += result * habits / 100;
		
		return (int) Math.round(result);
	}

	private static double getAgeBasedCalcuation(Customer customer, double result) {
		if(customer.getAge() >=18){
			result +=result * 10/100;
		}
		
		//% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% increase every 5 years
		if(customer.getAge() >=25 && customer.getAge() < 41){
			int i = 25;
			while (i <= customer.getAge()) {
				result += result * 10 / 100;
				i = i + 5;
			}
		}else{
			int i = 41;
			while (i <= customer.getAge()) {
				result += result * 20 / 100;
				i = i + 5;
			}
		}
		return result;
	}
	
	public static String getOutputString(Customer customer) {
		String title = customer.getGender() == 'm' ? "Mr. " : "Ms. ";
		StringBuffer output = new StringBuffer();
		output.append("Health Insurance Premium for ").append(title).append(customer.getLastName()).append(": Rs. ")
			.append(NumberFormat.getIntegerInstance().format(customer.getPremium()));

		return output.toString();
	}
	
}
