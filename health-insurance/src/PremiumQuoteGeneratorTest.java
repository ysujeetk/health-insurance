import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


public class PremiumQuoteGeneratorTest {

	private Customer customer;
	
	@Before
	public void setUp() throws Exception{
		
		customer = new Customer();
		customer.setFirstName("Norman");
		customer.setLastName("Gomes");
		customer.setAge(34);
		customer.setGender('m');
		//Current Health
		customer.setHypertension(false);
		customer.setBloodPressure(false);
		customer.setBloodSugar(false);
		customer.setOverweight(true);
		//Habits
		customer.setSmoking(false);
		customer.setAlcohol(true);
		customer.setDailyExercise(true);
		customer.setDrugs(false);
	}
	
	@Test
	public void testPremiumCalculation() {
		
		int premium = PremiumQuoteGenerator.premiumCalculation(customer);
		assertEquals(6856, premium);
		customer.setPremium(premium);
		
		assertEquals("Health Insurance Premium for Mr. Gomes: Rs. 6,856", PremiumQuoteGenerator.getOutputString(customer));
	}

}
